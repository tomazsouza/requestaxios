import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';

const style = StyleSheet.create({
    itemContainer: {
        borderWidth: 0.5,
        borderColor: '#999',
        margin: 10,
        padding: 10,
        flexDirection: 'row',
        backgroundColor: '#fff'
    },
    itemPhoto: {
        width: 102,
        height: 102,
    },
    detalhesItems: {
        flex: 1,
        marginLeft: 20
    },
    textTitle: {
        fontSize: 14,
        color: 'blue',
        marginBottom: 8
    },
    textValue: {
        fontSize: 14,
        fontWeight: 'bold'
    }
});

export default class Items extends Component {
    render() {
        return (
            <View style={style.itemContainer}>
                <View style={style.itemPhoto}>
                    <Image style={{height: 100, width: 100}} source={{uri: this.props.data.foto}}/>
                </View>
                <View style={style.detalhesItems}>
                    <Text style={style.textTitle}>{this.props.data.titulo}</Text>
                    <Text style={style.textValue}>R$ {this.props.data.valor}</Text>
                    <Text>Local: {this.props.data.local_anuncio}</Text>
                    <Text>Data de publicação{this.props.data.data_publicacao}</Text>
                </View>
            </View>
        );
    }
}