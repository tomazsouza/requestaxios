import React, {Component} from 'react';
import {View, Text} from 'react-native';
import axios from 'axios';
// Component App
import Items from './Items';

export default class ListItems extends Component {

    constructor(props) {
        super(props);
        this.state = {listItems: []};
    }

    componentWillMount() {

        const getItemsHttp = 'http://faus.com.br/recursos/c/dmairr/api/itens.html';
        const posts = 'https://jsonplaceholder.typicode.com/posts';
        // Request Http
        axios.get(getItemsHttp)
            .then(response => {
                this.setState({listItems: response.data})
            })
            .catch(error => {
                console.log('Erro ao recuperar os dados error');
            });
    }

    render() {
        return (
            <View>
                {this.state.listItems.map(mapItems => <Items key={mapItems.titulo} data={mapItems}/>)}
            </View>
        );
    }
}