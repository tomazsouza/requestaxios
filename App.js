import React, {Component} from 'react';
import {ScrollView, StyleSheet, View} from "react-native"

import ListItems from './src/components/ListItems'

const style = StyleSheet.create({
    background: {
        backgroundColor: '#ddd'
    }
});

export default class App extends Component {

    constructor(props) {
        super(props);
        console.log('Construindo a aplicação');
    }

    componentWillUnmount() {
        console.log('Fazer alguma coisa antes de renderizar');
    }

    componentDidMount() {
        console.log('Fazer alguma coisa depois da renderização');
    }

    render() {
        console.log('Objeto renderizado');
        return (
            <View>
                <ScrollView style={style.background}>
                    <ListItems/>
                </ScrollView>
            </View>
        );
    }
}
